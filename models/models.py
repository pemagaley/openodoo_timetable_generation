# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class timetable_generation(models.Model):
#     _name = 'timetable_generation.timetable_generation'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class Batch(models.Model):
    _inherit = 'op.batch'

    batch_ids = fields.Many2many('timetable_generation.generatetimetable',
                                 string='Generated Batches', readonly=True)
    session_ids = fields.One2many('op.session', 'batch_id')


class Course(models.Model):
    _inherit = 'op.course'

    course_ids = fields.Many2many('timetable_generation.generatetimetable',
                                 string='Generated Courses', readonly=True)
    batch_ids = fields.One2many('op.batch', 'course_id')

    classroom_ids = fields.One2many('op.classroom', 'course_id')


class Subject(models.Model):
    _inherit = 'op.subject'
    faculty_ids = fields.Many2many('op.faculty', string='Generated Faculty', readonly=True)

class Session(models.Model):
    _inherit = 'op.session'

    # generatedby = fields.Char(size=15, default='manual')
    day_of_week = fields.Char(string='Day', size=15)

    @api.multi
    @api.depends('start_datetime', 'day_of_week')
    def _compute_day(self):
        for record in self:
            if record.day_of_week == False:
                record.type = fields.Datetime.from_string(
                record.start_datetime).strftime("%A")
            else:
                record.type= record.day_of_week
