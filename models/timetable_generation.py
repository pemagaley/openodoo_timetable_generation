# -*- coding: utf-8 -*-
import calendar, random, json
from datetime import timedelta
from odoo import models, fields, api, exceptions, _

week_days = [(calendar.day_name[0]),
             (calendar.day_name[1]),
             (calendar.day_name[2]),
             (calendar.day_name[3]),
             (calendar.day_name[4]),
             # (calendar.day_name[5]),
             # (calendar.day_name[6])
            ]


class GenerateTimetable(models.Model):
    _name = 'timetable_generation.generatetimetable'

    session_name = fields.Char(string='Session Name', required=False)

    batch_ids = fields.Many2many('op.batch', string='Batches', required=True)
    course_ids = fields.Many2many('op.course', string='Courses', required=True)


    start_datetime = fields.Datetime(
        'Start Time', required=True,
        default=lambda self: fields.Datetime.now())

    end_datetime = fields.Datetime(
        'End Time', required=True)

    @api.onchange('course_ids')
    def _onchange_course_ids(self):
        print('OK')
        domain = {}
        course_list = []
        for course in self.course_ids:
            course_list.append(course.id)
        domain = {'batch_ids':[('course_id.id', 'in', course_list)]}
        batch_ids = []
        #for batch in self.batch_ids:
        #    print(self.course_ids.search(['id', '=', batch.course_id]))
        #    if self.course_ids.search(['id', '=', batch.course_id]):
        #        batch_ids.append(batch)
        return {'domain': domain, 'value': {'batch_ids': batch_ids}}


    # Check for complete free Day
    @api.multi
    def check_day_conflict(self, current_subject, current_timing, timing_ids, current_day, days, storage):
        result = True #By default, day is Conflicting
        result_whole_day = False
        result_day_time = False

        # Check whether whole day is Free or Not
        for i in range(len(days)):
            for j in range(len(timing_ids)):
                if days[i] == current_day and storage[i][j] is None:
                    result_whole_day = True
                else:
                    result_whole_day = False
                    break
            if result_whole_day is True:
                storage[i][0] = current_subject.name
                break


        if result_whole_day is True: # whole day is Free day
            result = False # Not Conflicting with day

        # print(storage)

        return result, storage

    # Check availability of both Day and Timing
    @api.multi
    def check_day_time_conflict(self, current_subject, current_timing, timing_ids, current_day, days, storage):
        result = True  # By default, day is Conflicting
        result_whole_day = False
        result_day_time = False

        # Check free time slots (both day and timing)
        for i in range(len(days)):
            for j in range(len(timing_ids)):
                if days[i] == current_day and timing_ids[j] == current_timing:
                    if storage[i][j] is None:
                        result_day_time = True
                        storage[i][j]=current_subject.name
                        break
                else:
                    result_day_time = False
            if result_day_time is True:
                result = False
                break

        # print(storage)
        return result, storage

    # Check availibility of Faculty
    @api.multi
    def check_faculty_conflict(self, subject_id, current_day, current_timing):
        print('Faculty Name: {0}'.format(subject_id.faculty_ids.name))
        session_obj = self.env['op.session'].search([])
        faculty_conflict_result = False
        # session_len = len(session_obj)
        # store_day_time = [[None for _ in range(2)] for _ in range(session_len)]
        store_day = []
        store_time = []
        for session_id in session_obj:
            # print('Faculty Name(s): {0}'.format(session_id.faculty_id.name))
            # print('Day : {0}'.format(session_id.type))
            # print('Time : {0}'.format(session_id.timing_id.name))
            if session_id.faculty_id == subject_id.faculty_ids:
                store_day.append(session_id.type)
                store_time.append(session_id.timing_id)

        for i in range(len(store_day)):
            print('Day : {0}'.format(store_day[i]))
            print('Time : {0}'.format(store_time[i]))
            if store_day[i] == current_day and store_time[i] == current_timing:
                faculty_conflict_result = True
                break

        print('Current Day : {0}'.format(current_day))
        print('Current Timing : {0}'.format(current_timing.name))
        print('Faculty Conflict : {0}'.format(faculty_conflict_result))
        return faculty_conflict_result


    # For record rule on student and faculty dashboard
    @api.one
    def compute_timetable_generation(self):
        print('Timetable Generation\n------------------------------------')
        session_name_object = self.session_name
        batch_obj = self.batch_ids
        course_obj = self.course_ids
        start_datetime_obj = self.start_datetime
        end_datetime_obj = self.end_datetime
        timing_obj = self.env['op.timing'].search([])

        #Logic for Timetable Generation
        print('--------------SESSIONS-----------------')
        # Generate selected batch list
        for batch_id in batch_obj:
            batch_name = batch_id.name

            # Generate course list
            for course_id in batch_id.course_id:
                course_name = course_id.name

                # Temporary storage of Data
                days_len = len(week_days)  # number of rows
                timing_len = len(timing_obj) # number of columns
                subject_assign = [[None for _ in range(timing_len)] for _ in range(days_len)]

                # Generate classroom assigned to particular course
                classroom_id = None
                classroom =" "
                for classroom_idd in course_id.classroom_ids:
                    classroom = classroom_idd.name
                    classroom_id = classroom_idd


                # Generate subject list under particular course
                # subjectids = course_id.subject_ids
                # print('Subject List: {0}'.format(subjectids))
                # random.shuffle(subjectids)
                # print('Shuffled List: {0}'.format(subjectids))
                for subject_id in course_id.subject_ids:
                    print('--------------------------------------')
                    subject_name = subject_id.name

                    # Generate assigned faculty
                    faculty_id = None
                    faculty_name = ""
                    for faculty_idd in subject_id.faculty_ids:
                        faculty_name = faculty_idd.name
                        faculty_id = faculty_idd

                    # Main loop to  check for free day, time and faculty
                    day = random.choice(week_days) # Generate random day
                    timing_random = random.choice(timing_obj) # Generate timing / period = Morning or Afternoon
                    # Flags
                    flag = True
                    result_day = False
                    result_day_time = False
                    result_faculty = False
                    day_conflict_count = []
                    while flag:
                        # Check for complete Free Day
                        result_day, subject_assign = self.check_day_conflict(subject_id, timing_random, timing_obj,
                                                                             day, week_days, subject_assign)
                        if result_day is True: # Conflicted with another Day
                            day_conflict_count.append(day)
                            day = random.choice(week_days)
                            timing_random = random.choice(timing_obj)
                            flag = True
                        else:  # No confliction with Day and by default Timing is Morning [0]
                            timing_random = timing_obj[0]  # Morning Class
                            # check for Faculty free or not
                            result_faculty = self.check_faculty_conflict(subject_id, day, timing_random)
                            if result_faculty is True:
                                day = random.choice(week_days)
                                timing_random = random.choice(timing_obj)
                                flag = True
                            else:
                                flag = False
                                break

                        # Whole week days are booked/conflicted
                        # print(day_conflict_count)
                        # print(set(day_conflict_count))
                        # print(len(set(day_conflict_count)))
                        if len(set(day_conflict_count)) >= len(week_days) and flag is not False:
                            print("check for both day and timing combination available..")
                            result_day_time, subject_assign = self.check_day_time_conflict(subject_id, timing_random,
                                                                                           timing_obj,day, week_days,
                                                                                           subject_assign)
                            if result_day_time is True: # Conflicted Day and Timing Combination
                                day = random.choice(week_days)
                                timing_random = random.choice(timing_obj)
                            else:  # Not Confliction for Day and Timing
                                # check for Faculty free or not
                                result_faculty = self.check_faculty_conflict(subject_id, day, timing_random)
                                if result_faculty is True:
                                    day = random.choice(week_days)
                                    timing_random = random.choice(timing_obj)
                                    flag = True
                                else:
                                    flag = False
                                    break



                    # OpSession object
                    OPSession = self.env['op.session'].create({
                        'timing_id': timing_random.id, 'start_datetime':start_datetime_obj, 'end_datetime':end_datetime_obj,
                        'course_id': course_id.id, 'faculty_id':faculty_id.id, 'batch_id': batch_id.id,
                        'subject_id': subject_id.id, 'classroom_id': classroom_id.id, 'day_of_week': day
                    })

                print('----------------------------------')